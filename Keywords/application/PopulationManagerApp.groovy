package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords


import keywordsLibrary.newkeyword
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import org.apache.commons.lang.StringUtils;
import org.junit.After
public class PopulationManagerIcon {
	@Keyword
	public Boolean PopulationManagerAppNavigation(){
		try{
			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Common/Pop_Mgr_icon'), 5, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(2)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Common/Pop_Mgr_icon'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(2)
			WebUI.switchToWindowIndex(1)
			//def Title=TestDataFactory.findTestData("PopulationManager/CommonParameter").getValue("Title", 1)
			WebUI.delay(2)
			WebUI.setViewPortSize(1920, 1080)
			WebUI.waitForPageLoad(60, FailureHandling.STOP_ON_FAILURE)
			Boolean PopMangerNavigationStatus=WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_HomeButton'), 5, FailureHandling.OPTIONAL)
			return PopMangerNavigationStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean StandardOperationReportNavigation(){
		try {
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_HomeButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(4)
			WebUI.mouseOver(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_DemoBrowseButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_DemoBrowseButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementClickable(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_PublicButton'), 60, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_Public_ExpandButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), 6, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), FailureHandling.STOP_ON_FAILURE)
			Boolean StandardOperationReportNavigation=WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), 3, FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), 6, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'),FailureHandling.STOP_ON_FAILURE)
			return StandardOperationReportNavigation
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	def boolean isNumericValue(String str){
		return StringUtils.isNumeric(str);
	}
}

