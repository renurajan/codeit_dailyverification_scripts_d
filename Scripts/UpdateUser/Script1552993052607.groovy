import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import keywordsLibrary.CommomLibrary
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException

ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false

def address_1 = CustomKeywords.'commonutils.commonkeyword.randomTextalone'()

//Click AdminApp Icon
test.log(LogStatus.INFO,"Navigate to Admin App")
WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/AdminApp/AdminAppIcon'),10, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Object Repository/Applications/AdminApp/AdminAppIcon'), FailureHandling.STOP_ON_FAILURE)

//click UpdateUser Icon
WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPageIcon'),10,FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPageIcon'),FailureHandling.STOP_ON_FAILURE)

//Send UserID as Input for UserSearch
WebUI.clearText(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPage/AA_UUP_UserEmail'),FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPage/AA_UUP_UserEmail'),userID,FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)
WebUI.click(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPage/AA_UUP_User_Search'),FailureHandling.STOP_ON_FAILURE)

//User Select
test.log(LogStatus.INFO,"Search user for Address1 Modification")
TestObject userSelect = findTestObject('Base/commanXpath')
userSelect.findProperty('xpath').setValue("//tr[@role='listitem']/td/div[text()='"+userLastName+"']/..//following-sibling::td/div[text()='"+userFirstName+"']")
WebUI.verifyElementClickable(userSelect,FailureHandling.STOP_ON_FAILURE)
WebUI.click(userSelect,FailureHandling.STOP_ON_FAILURE)

def UI_FirstName = WebUI.getText(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPage/AA_GetText_FirstName'),FailureHandling.CONTINUE_ON_FAILURE)
def UI_LastName = WebUI.getText(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPage/AA_GetText_LastName'),FailureHandling.CONTINUE_ON_FAILURE)
def UI_Address1 = WebUI.getText(findTestObject('Object Repository/Applications/AdminApp/UpdateUserPage/AA_GetText_Address1'),FailureHandling.CONTINUE_ON_FAILURE)
test.log(LogStatus.INFO,"Modify Address 1 Field")
if((UI_FirstName.equals(userFirstName))||(UI_LastName.equals(userLastName))||(UI_Address1!= null)){
	WebUI.scrollToElement(findTestObject("Object Repository/Applications/AdminApp/UpdateUserPage/AA_GetText_Address1"), 10,FailureHandling.STOP_ON_FAILURE)
	WebUI.setText(findTestObject("Object Repository/Applications/AdminApp/UpdateUserPage/AA_GetText_Address1"), address_1,FailureHandling.STOP_ON_FAILURE)
	WebUI.scrollToElement(findTestObject("Object Repository/Applications/AdminApp/UpdateUserPage/AA_MPP_SaveButton"), 10,FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject("Object Repository/Applications/AdminApp/UpdateUserPage/AA_MPP_SaveButton"),10,FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject("Object Repository/Applications/AdminApp/UpdateUserPage/AA_MPP_SaveButton"),FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject("Object Repository/Applications/AdminApp/UpdateUserPage/AA_MPP_SaveSuccessfullPopupOkButton"),10,FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject("Object Repository/Applications/AdminApp/UpdateUserPage/AA_MPP_SaveSuccessfullPopupOkButton"),FailureHandling.STOP_ON_FAILURE)
}	

GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
test.log(LogStatus.INFO,"TestCase is executed successfully")

}
}catch(StepFailedException e){
test.log(LogStatus.FAIL, e.message)
CustomKeywords.'reports.extentReports.takeScreenshot'(test)
throw e

}


