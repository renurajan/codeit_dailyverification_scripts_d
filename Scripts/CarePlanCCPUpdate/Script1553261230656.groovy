import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
		def issuesTitle1 =CustomKeywords.'keywordsLibrary.CommomLibrary.randomTextalone'()
		def objectiveGoal1=CustomKeywords.'keywordsLibrary.CommomLibrary.randomTextalone'()
		def interventionPlan1=CustomKeywords.'keywordsLibrary.CommomLibrary.randomTextalone'()
		
//Navigate to Careplan App
	test.log(LogStatus.INFO, "Navigate to care plan app")
WebUI.click(findTestObject('Application/CarePlanApp/AppcareplanIcon'), FailureHandling.STOP_ON_FAILURE)
WebUI.switchToWindowIndex(1)
WebUI.maximizeWindow()
WebUI.delay(8)

//Navigate to Search PatientPage
WebUI.verifyElementPresent(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientSearch'), 15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientSearch'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(10)
WebUI.verifyElementPresent(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), 15, FailureHandling.OPTIONAL)

WebUiCommonHelper.findWebElement(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), 3).clear()
WebUI.setText(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), patientID, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_lookUP'), FailureHandling.STOP_ON_FAILURE)

//Verify Patient searched patient displayed
test.log(LogStatus.INFO,"Selected Patient Search")
WebUI.delay(6)
TestObject patientSearchObject=findTestObject('Base/commanXpath')
patientSearchObject.findProperty('xpath').setValue("(//table[@class='dataVal'])[3]//td[contains(text(),'"+patientLastName+"')]")
WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)

WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP_EncounterInformation'),5, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP_EncounterInformation'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/CCPButton'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/CCPButton'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)

test.log(LogStatus.INFO, "Update CCP issue")
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/showFilterIconInHomePage'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/showFilterIconInHomePage'),FailureHandling.STOP_ON_FAILURE)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/selectAllStatusInFilter'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/selectAllStatusInFilter'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/searchCCPKeyword'),15, FailureHandling.STOP_ON_FAILURE)
WebUI.sendKeys(findTestObject('Object Repository/Application/CarePlanApp/CCP/searchCCPKeyword'), issuesTitle , FailureHandling.STOP_ON_FAILURE)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/applyFilterButton'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/applyFilterButton'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/existingCCP'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/existingCCP'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/Title'),15, FailureHandling.OPTIONAL)
WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/CarePlanApp/CCP/Title'), 5).clear()
WebUI.setText(findTestObject('Object Repository/Application/CarePlanApp/CCP/Title'),issuesTitle1 , FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)

test.log(LogStatus.INFO, "Update CCP goal")
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/goalSectionPlusMinus'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/goalSectionPlusMinus'),FailureHandling.STOP_ON_FAILURE)
if(WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/existingGoalCheck'), 4, FailureHandling.OPTIONAL))	{
	// Update existing Goal
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/goalWindowExpand'),FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/editGoalIcon'),15, FailureHandling.OPTIONAL)
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/editGoalIcon'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(3)
	WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/CarePlanApp/CCP/objectiveGoal'), 5).clear()
	WebUI.setText(findTestObject('Object Repository/Application/CarePlanApp/CCP/objectiveGoal'),objectiveGoal1, FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/saveGoal'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	
	
}
else{
	test.log(LogStatus.INFO, "Create CCP goal")
	//Add new Goal
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/addGoalButton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/GoalStartDateCalender'),15, FailureHandling.OPTIONAL)
	
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/GoalStartDateCalender'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	WebUI.sendKeys(findTestObject('Object Repository/Application/CarePlanApp/CCP/goalStartDate'),  goalStartDate , FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	
	WebUI.click(findTestObject("Object Repository/Application/CarePlanApp/CCP/goalType"),FailureHandling.STOP_ON_FAILURE)
	def visitDropDownTypeValue1="//option[text()[normalize-space() ='"+goalType+"']]"
	WebUI.delay(2)
	WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(visitDropDownTypeValue1))
	
	//goalStatus
	WebUI.click(findTestObject("Object Repository/Application/CarePlanApp/CCP/goalStatus"),FailureHandling.STOP_ON_FAILURE)
	def visitDropDownTypeValue2="//option[text()[normalize-space() ='"+goalStatus+"']]"
	WebUI.delay(2)
	WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(visitDropDownTypeValue2))
	
	//objectiveGoal
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/objectiveGoal'),FailureHandling.STOP_ON_FAILURE)
	WebUI.setText(findTestObject('Object Repository/Application/CarePlanApp/CCP/objectiveGoal'),  objectiveGoal , FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	
	WebUI.delay(4)
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/saveGoal'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
}
test.log(LogStatus.INFO, "Update CCP intervention")
//Update Intervention
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionSectionPlusMinus'),15, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionSectionPlusMinus'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)
if(WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/existingInterventionCheck'), 4, FailureHandling.OPTIONAL))	{
	// Update existing Intervention
		
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionWindowExpand'),FailureHandling.STOP_ON_FAILURE)
	verifyStatus=WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/editInterventionIcon'),15, FailureHandling.OPTIONAL)
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/editInterventionIcon'),FailureHandling.STOP_ON_FAILURE)
	//interventionPlan1
	WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionPlan'), 5).clear()
	WebUI.setText(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionPlan'), interventionPlan1, FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/saveIntervention'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
}
else{
	test.log(LogStatus.INFO, "Create CCP intervention")
	//Add new Intervention
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/addInterventionButton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionStatus'),15, FailureHandling.OPTIONAL)
	//interventionStatus
	WebUI.click(findTestObject("Object Repository/Application/CarePlanApp/CCP/interventionStatus"),FailureHandling.STOP_ON_FAILURE)
	def visitDropDownTypeValue3="//option[text()[normalize-space() ='"+interventionStatus+"']]"
	WebUI.delay(2)
	WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(visitDropDownTypeValue3))
	//interventionPlan
	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionPlan'),FailureHandling.STOP_ON_FAILURE)
	WebUI.setText(findTestObject('Object Repository/Application/CarePlanApp/CCP/interventionPlan'),  interventionPlan , FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)

	WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/saveIntervention'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	
}

WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/saveCCP'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/okButtonInConfirmPopup'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/okButtonInConfirmPopup'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)

// Verify Updated issue name '"+issuesTitle1+"'
WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td//a[contains(text(),'"+issuesTitle1+"')]"), 10, FailureHandling.OPTIONAL)
WebUI.delay(3)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/existingCCP'),FailureHandling.OPTIONAL)
WebUI.delay(3)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/Title'),15, FailureHandling.OPTIONAL)
//issuesTitle
WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/CarePlanApp/CCP/Title'), 5).clear()
WebUI.setText(findTestObject('Object Repository/Application/CarePlanApp/CCP/Title'), issuesTitle, FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/saveCCP'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/saveCCP'),FailureHandling.OPTIONAL)
WebUI.delay(3)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/CCP/okButtonInConfirmPopup'),15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/CCP/okButtonInConfirmPopup'),FailureHandling.OPTIONAL)
WebUI.delay(3)
WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td//a[contains(text(),'"+issuesTitle+"')]"), 10, FailureHandling.OPTIONAL)

WebUI.closeWindowIndex(1)
WebUI.switchToDefaultContent()
WebUI.delay(5)
GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
test.log(LogStatus.PASS,"TestCase is executed successfully")
	}

}catch(StepFailedException e){
	test.log(LogStatus.FAIL, e.message)
	CustomKeywords.'reports.extentReports.takeScreenshot'(test)
	throw e
	
}