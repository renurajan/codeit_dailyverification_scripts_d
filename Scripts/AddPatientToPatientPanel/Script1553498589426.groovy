import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import keywordsLibrary.CommomLibrary
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
		
		test.log(LogStatus.INFO,"Search patient in patient panel with id")
		//Search the patient is present in the Patient Panel
		def searchPatientAndRemovePatientInPanelResult = CustomKeywords.'application.PatientPanel.removePatientInThePatientPanel'(patientID,patientLastName)
	    WebUI.verifyMatch(searchPatientAndRemovePatientInPanelResult, "NoPatientRecordFound", false,FailureHandling.OPTIONAL)
		
		//Navigate to Demographics App
		test.log(LogStatus.INFO,"Navigate to Demographic app and Search patient")
		WebUI.verifyElementPresent(findTestObject('Applications/Demographics/DemographicsIcon'), 5, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Applications/Demographics/DemographicsIcon'), FailureHandling.STOP_ON_FAILURE)
		WebUI.verifyElementPresent(findTestObject('Applications/Demographics/SearchPatientPage/DA_SPP_searchButton'), 5, FailureHandling.OPTIONAL)
		
		//Search a patient in the demographics app
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/PatientPanelSearch/patientID'), 5).clear()
		WebUI.setText(findTestObject('Object Repository/Application/PatientPanelSearch/patientID'), patientID, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(2)
		
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/PatientPanelSearch/PatientLastName'), 5).clear()
		WebUI.setText(findTestObject('Object Repository/Application/PatientPanelSearch/PatientLastName'), patientLastName, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(2)
		
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/PatientPanelSearch/PatientFirstName'), 5).clear()
		WebUI.setText(findTestObject('Object Repository/Application/PatientPanelSearch/PatientFirstName'), patientFirstName, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(2)
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/PatientPanelSearch/SelectButton'), 10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Application/PatientPanelSearch/SelectButton'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(4)
		
		
		WebUI.delay(4)
		TestObject patientSearchObject3 = findTestObject('Base/commanXpath')
		patientSearchObject3.findProperty('xpath').setValue("//div[text()='" +patientID+"']")
		Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject3, 15, FailureHandling.OPTIONAL)
		WebUI.delay(3)
		WebUI.click(patientSearchObject3, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		
		//NAVIGATE TO VIEW PATIENT PAGE
		WebUI.scrollToElement(findTestObject("Applications/Demographics/SearchPatientPage/DA_SPP_selectButton"), 10,FailureHandling.STOP_ON_FAILURE)
		WebUI.verifyElementPresent(findTestObject('Applications/Demographics/SearchPatientPage/DA_SPP_selectButton'), 10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Applications/Demographics/SearchPatientPage/DA_SPP_selectButton'), FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(4)
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/PatientPanelSearch/AddToPatientPanelButton'), 10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Application/PatientPanelSearch/AddToPatientPanelButton'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/PatientPanelSearch/AddToPatientPanelOK'), 10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Application/PatientPanelSearch/AddToPatientPanelOK'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(5)
		
	    //Remove from the panel
		test.log(LogStatus.INFO,"Remove Added patient from the patient panel")
		searchPatientAndRemovePatientInPanelResult = CustomKeywords.'application.PatientPanel.removePatientInThePatientPanel'(patientID,patientLastName)
		verifyStatus=WebUI.verifyMatch(searchPatientAndRemovePatientInPanelResult, "NoPatientRecordFound", false,FailureHandling.OPTIONAL)
		
		WebUI.click(findTestObject("Object Repository/LoginPage/homePageVerification"),FailureHandling.STOP_ON_FAILURE)
		GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
		test.log(LogStatus.INFO,"TestCase is executed successfully")
		
	}
}catch(StepFailedException e){
		test.log(LogStatus.FAIL, e.message)
		CustomKeywords.'reports.extentReports.takeScreenshot'(test)
		throw e
		
	}
			
