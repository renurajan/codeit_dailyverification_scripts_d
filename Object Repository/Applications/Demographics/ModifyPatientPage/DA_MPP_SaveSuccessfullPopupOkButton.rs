<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_MPP_SaveSuccessfullPopupOkButton</name>
   <tag></tag>
   <elementGuidId>35cdb7b6-e618-4896-bf0f-5d0acd05dec0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Save successful.']//following::div[text()='OK']</value>
   </webElementProperties>
</WebElementEntity>
