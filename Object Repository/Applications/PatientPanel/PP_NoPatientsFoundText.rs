<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PP_NoPatientsFoundText</name>
   <tag></tag>
   <elementGuidId>735f3655-dcd5-4dae-817b-4ed246ee85ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),'No patients found.')] | //div[contains(text(),'No members found.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'No patients found.')] | //div[contains(text(),'No members found.')]</value>
   </webElementProperties>
</WebElementEntity>
