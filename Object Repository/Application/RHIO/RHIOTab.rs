<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RHIOTab</name>
   <tag></tag>
   <elementGuidId>ce9e973c-e844-4815-8977-795646f289be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-tab-item/span[contains(text(),'RHIO Sharing')]</value>
   </webElementProperties>
</WebElementEntity>
