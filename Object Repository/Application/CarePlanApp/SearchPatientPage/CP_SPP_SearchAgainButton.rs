<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_SPP_SearchAgainButton</name>
   <tag></tag>
   <elementGuidId>b1e25b92-bae1-4bfb-915b-6d10eb7a467b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@value=&quot;Search Again&quot;]</value>
   </webElementProperties>
</WebElementEntity>
