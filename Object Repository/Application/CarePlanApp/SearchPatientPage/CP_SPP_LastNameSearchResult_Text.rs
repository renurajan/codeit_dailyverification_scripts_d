<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_SPP_LastNameSearchResult_Text</name>
   <tag></tag>
   <elementGuidId>2e50b7e7-13e9-43a4-b6ea-6e39ddba7b2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[contains(text(),&quot;Search Results for Last Name&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
