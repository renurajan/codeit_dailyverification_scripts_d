<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_SPP_NoResultsFound</name>
   <tag></tag>
   <elementGuidId>0fb080a9-393c-4adf-8151-f516d633cb08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(text(),'No Results Found')]</value>
   </webElementProperties>
</WebElementEntity>
