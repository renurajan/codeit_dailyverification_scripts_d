<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CA_ViewPatientPageText</name>
   <tag></tag>
   <elementGuidId>8411baf0-d00c-42db-9458-e06d09be285d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='Patient Demographics']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='Patient Demographics']</value>
   </webElementProperties>
</WebElementEntity>
