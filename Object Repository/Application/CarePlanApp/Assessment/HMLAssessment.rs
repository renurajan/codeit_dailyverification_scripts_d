<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>HMLAssessment</name>
   <tag></tag>
   <elementGuidId>9141a594-cfc7-415d-bb3b-00e47c1be328</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//font[contains(text(), 'HML Assessment')]/../..//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//font[contains(text(), 'HML Assessment')]/../..//input</value>
   </webElementProperties>
</WebElementEntity>
