<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_UpdateUser_Click</name>
   <tag></tag>
   <elementGuidId>96a97880-115e-4c2a-9d35-86e183cb6ad5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[text()='Update User']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Update User']</value>
   </webElementProperties>
</WebElementEntity>
