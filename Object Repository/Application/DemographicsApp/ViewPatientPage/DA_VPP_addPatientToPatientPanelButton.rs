<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_VPP_addPatientToPatientPanelButton</name>
   <tag></tag>
   <elementGuidId>162058c5-2284-43b1-b6a7-11c193ac6d2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(text(),'Add To Patient Panel')]</value>
   </webElementProperties>
</WebElementEntity>
