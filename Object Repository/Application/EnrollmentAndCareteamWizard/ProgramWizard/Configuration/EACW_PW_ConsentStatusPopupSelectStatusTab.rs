<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_ConsentStatusPopupSelectStatusTab</name>
   <tag></tag>
   <elementGuidId>2eac80f8-abb0-4b2c-910a-2eeabb659102</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-select-menu/md-content/md-option/div[contains(text(),&quot;Select Status&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
