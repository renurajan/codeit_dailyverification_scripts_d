<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_SP_DelegateEmailInput</name>
   <tag></tag>
   <elementGuidId>ec6dfc61-c383-4dc0-8aed-764b53e0e311</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='Delegate Email :']/../following-sibling::td/input[@type='text']</value>
   </webElementProperties>
</WebElementEntity>
