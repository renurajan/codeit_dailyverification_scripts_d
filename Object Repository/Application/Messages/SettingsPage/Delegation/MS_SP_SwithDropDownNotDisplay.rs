<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_SP_SwithDropDownNotDisplay</name>
   <tag></tag>
   <elementGuidId>c4a388c0-9c9f-4c4f-b45f-ccc6046f26cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@class='gwt-ListBox' and @style='display: none;']</value>
   </webElementProperties>
</WebElementEntity>
