<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_CNMP_Sendbutton</name>
   <tag></tag>
   <elementGuidId>a18ccb02-2e81-4b40-882e-0a6d3073641e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[@class=&quot;hupa-commands-bar&quot;]//following::tr/td/div/button[contains (text(),'Send')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[@class=&quot;hupa-commands-bar&quot;]//following::tr/td/div/button[contains (text(),'Send')]</value>
   </webElementProperties>
</WebElementEntity>
