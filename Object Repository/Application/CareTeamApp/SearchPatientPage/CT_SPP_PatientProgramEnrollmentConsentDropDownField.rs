<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CT_SPP_PatientProgramEnrollmentConsentDropDownField</name>
   <tag></tag>
   <elementGuidId>b2e2cfac-c0e1-4c0f-8562-cadfe7699bfd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;Program Enrollment Consent&quot;]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;Program Enrollment Consent&quot;]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
   </webElementProperties>
</WebElementEntity>
