import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/Model/NFMMC')

suiteProperties.put('name', 'NFMMC')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\qatester4\\Desktop\\Dailyprod_DD\\DailyProdDataDriven\\Reports\\Model\\NFMMC\\20190401_120244\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/Model/NFMMC', suiteProperties, [new TestCaseBinding('Test Cases/Login', 'Test Cases/Login',  [ 'userID' : 'prabhaharan.velu@gsihealth.com' , 'url' : 'https://nfmmc.gsihealth.com' , 'ENV' : 'NFMMC' , 'password' : 'Imax123#' ,  ]), new TestCaseBinding('Test Cases/AddNewProgram', 'Test Cases/AddNewProgram',  [ 'patientID' : '7157' , 'patientLastName' : 'Bram' ,  ])])
